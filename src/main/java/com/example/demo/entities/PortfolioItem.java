package com.example.demo.entities;

public class PortfolioItem {
	private String stockTicker;
	private double price;
	private int volume;
	private String buyOrSell;
	private int statusCode;
	private String datetime;
	
	public PortfolioItem(String stockTicker, double price, int volume, String buyOrSell, int statusCode,
			String datetime) {
		super();
		this.stockTicker = stockTicker;
		this.price = price;
		this.volume = volume;
		this.buyOrSell = buyOrSell;
		this.statusCode = statusCode;
		this.datetime = datetime;
	}
	
	public PortfolioItem() {
		// TODO Auto-generated constructor stub
	}

	public String getStockTicker() {
		return stockTicker;
	}
	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public String getBuyOrSell() {
		return buyOrSell;
	}
	public void setBuyOrSell(String buyOrSell) {
		this.buyOrSell = buyOrSell;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	public String getDatetime() {
		return datetime;
	}
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	
	
}

