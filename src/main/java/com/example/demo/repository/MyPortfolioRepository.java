package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.PortfolioItem;

@Repository
public class MyPortfolioRepository implements PortfolioRepository {
	@Autowired
	JdbcTemplate template;
	
	@Override
	public List<PortfolioItem> getAllPortfolioItems(){
		String sql = "SELECT datetime, stockTicker, price, volume, buyOrSell, statusCode FROM portfolio";
		return (List<PortfolioItem>) template.query(sql, new PortfolioItemRowMapper());
	}
	
	class PortfolioItemRowMapper implements RowMapper<PortfolioItem> {

		@Override
		public PortfolioItem mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new PortfolioItem( rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),
					rs.getString("buyOrSell"), rs.getInt("statusCode"), rs.getString("updatedtime"));

		}

	}
}
