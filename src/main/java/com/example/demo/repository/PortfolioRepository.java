package com.example.demo.repository;

import java.util.List;

import org.springframework.stereotype.Component;

import com.example.demo.entities.PortfolioItem;

@Component
public interface PortfolioRepository {
	public List<PortfolioItem> getAllPortfolioItems();
}
